export class CreatePost {
  post_title: string;
  post_image: string;
  post_contents: string;
  // Initialize the value

  constructor( post_title: string,
    post_image: string,
    post_contents: string,) {
    this.post_title = post_title;
    this.post_image = post_image;
    this.post_contents = post_contents;
  }
}
