import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CreatePost} from "../dto/create-post";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private url: string = 'https://taw-posts.herokuapp.com';

  constructor(private http: HttpClient) { }

  getById(id: string) {
    return this.http.get(this.url + '/api/posts/' + id);
  }

  getAll() {
    return this.http.get(this.url + '/api/posts');
  }

  postOne(createPost: CreatePost){
    return this.http.post<CreatePost>(this.url+'/api/create-post', createPost);
  }

}
