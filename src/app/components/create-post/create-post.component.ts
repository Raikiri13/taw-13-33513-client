import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {

  public credentials = {
    post_title: '',
    post_image: '',
    post_contents: '',
  };

  constructor(public router: Router) { }

  ngOnInit(): void {
  }

  create(){

    this.router.navigate(['/blog']);
  }

}
